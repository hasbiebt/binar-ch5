package com.practice.challenge_5.api

import com.practice.challenge_5.model.CartPost
import com.practice.challenge_5.model.MenuResponse
import com.practice.challenge_5.model.OrderResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface APIService {
    @GET("listmenu")
    fun getListMenu(): Call<MenuResponse>

    @POST("order")
    fun order(@Body orderData: CartPost): Call<OrderResponse>

}